from django.test import TestCase,Client

# Create your tests here.
class test(TestCase):
    def test_apakah_url_book_ada(self):
        response = Client().get('/book/')
        self.assertEquals(response.status_code, 200)

    def test_apakah_di_halaman_book_ada_templatenya(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'Book/search.html','base.html')
        
