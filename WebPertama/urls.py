from django.urls import path

from . import views

app_name = 'WebPertama'

urlpatterns = [
    path('', views.WebPertama, name='FirstWeb'),
]