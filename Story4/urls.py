"""Story4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from Book.views import pengolahan_data,tampilan

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', include('main.urls')),
    path('UI/',include('UI.urls')),
    path('FirstWeb/',include('WebPertama.urls')),
    path('Detail/',include('Detail.urls')),
    path('Kegiatan/',include('Kegiatan.urls')),
    path('book/',tampilan),
    path('data/',pengolahan_data),
    
]
