from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from .models import kegiatan,Peserta
# Create your views here.

class KegiatanList(ListView): 
  
    # specify the model for list view 
    model = kegiatan

class KegiatanCreate(CreateView): 
  
    # specify the model for create view 
    model = kegiatan
  
    # specify the fields to be displayed 
  
    fields = ['Nama']

class PesertaCreate(CreateView): 
  
    # specify the model for create view 
    model = Peserta
  
    # specify the fields to be displayed 
  
    fields = ['Nama','Kegiatan']

def detail_view(request, id): 
	# dictionary for initial data with 
	# field names as keys 
	context ={} 

	# add the dictionary during initialization 
	context = kegiatan.objects.get(id = id).Peserta_set.all()
		
	return render(request, "Kegiatan/detail_view.html", context) 