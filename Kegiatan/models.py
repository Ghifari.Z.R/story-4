from django.db import models

class kegiatan(models.Model): 
  
    # fields of the model 
    Nama= models.CharField(max_length = 200) 

    # renames the instances of the model 
    # with their title name 
    def __str__(self): 
        return self.Nama

    def get_absolute_url(self):
        return f"/Kegiatan/"
class Peserta(models.Model): 
  
    # fields of the model 
    Nama= models.CharField(max_length = 200)
    Kegiatan= models.ForeignKey(kegiatan, on_delete=models.CASCADE) 
     
    # renames the instances of the model 
    # with their title name 
    def __str__(self): 
        return self.Nama

    
    def get_absolute_url(self):
        return f"/Kegiatan/"

