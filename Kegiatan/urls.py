from django.urls import path
from . import views 
from .views import KegiatanList, KegiatanCreate,PesertaCreate,detail_view


app_name = 'Kegiatan'


urlpatterns = [ 
    path('',KegiatanList.as_view()),
    path('TambahKegiatan/', KegiatanCreate.as_view()),
    path('<id>/TambahPeserta',PesertaCreate.as_view()),
    path('<id>', detail_view ), 
] 
