from django.test import TestCase, Client
from .models import kegiatan,Peserta

# Create your tests here.
class Testing123(TestCase):
    def test_apakah_url_kegiatan_ada(self):
        response = Client().get('/Kegiatan/')
        self.assertEquals(response.status_code, 200)
    
    def test_apakah_di_halaman_kegiatan_ada_List_kegiatan_dan_tombol_tambahkegiatan(self):
        response = Client().get('/Kegiatan/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("List Kegiatan", html_kembalian)
        self.assertIn("Tambah Kegiatan", html_kembalian)

    def test_apakah_di_halaman_Kegiatan_ada_templatenya(self):
        response = Client().get('/Kegiatan/')
        self.assertTemplateUsed(response, 'Kegiatan/kegiatan_list.html','base.html')

    def test_apakah_url_TambahKegiatan_ada(self):
        response = Client().get('/Kegiatan/TambahKegiatan/')
        self.assertEquals(response.status_code, 200)