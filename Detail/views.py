from django.shortcuts import render
from django.views.generic.list import ListView 
from django.views.generic.edit import CreateView 
from django.views.generic.edit import DeleteView 
from .models import JadwalModel 


  
class JadwalList(ListView): 
  
    # specify the model for list view 
    model = JadwalModel 
  
class JadwalCreate(CreateView): 
  
    # specify the model for create view 
    model = JadwalModel 
  
    # specify the fields to be displayed 
  
    fields = ['Matkul','Dosen','SKS', 'description','Tahun','Kelas'] 

  

  
class JadwalDeleteView(DeleteView): 
    # specify the model you want to use 
    model = JadwalModel 
      
    # can specify success url 
    # url to redirect after sucessfully 
    # deleting object 
    success_url ="Detail/"

    

# pass id attribute from urls 
def detail_view(request, ID): 
	# dictionary for initial data with 
	# field names as keys 
	context ={} 

	# add the dictionary during initialization 
	context["data"] = JadwalModel.objects.get(id = ID) 
		
	return render(request, "Detail/detail_view.html", context) 
