from django.urls import path
from .views import JadwalList 
from .views import JadwalCreate 
from .views import JadwalDeleteView  
from .views import detail_view 
from . import views 



app_name = 'Detail'


urlpatterns = [ 
    path('', JadwalList.as_view()), 
    path('TambahJadwal/', JadwalCreate.as_view()),
    path('<pk>/delete/', JadwalDeleteView.as_view()),
    path('<ID>', detail_view ), 
] 
