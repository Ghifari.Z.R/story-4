# import the standard Django Model 
# from built-in library 
from django.db import models 
from django.urls import reverse
   
# declare a new model with a name "GeeksModel" 
class JadwalModel(models.Model): 
  
    # fields of the model 
    Matkul = models.CharField(max_length = 200) 
    Dosen = models.CharField(max_length = 200) 
    SKS = models.IntegerField(max_length = 200) 
    description = models.TextField() 
    Tahun = models.CharField(max_length = 200) 
    Kelas = models.CharField(max_length = 200) 
    # renames the instances of the model 
    # with their title name 
    def __str__(self): 
        return self.Matkul

    def get_absolute_url(self):
        return f"/Detail/"